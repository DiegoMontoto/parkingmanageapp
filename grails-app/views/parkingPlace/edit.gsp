<%--
  Created by IntelliJ IDEA.
  User: diegomontoto
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="edit.place.title"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
</head>

<body>
<div>
    <h1>HEADER</h1>
</div>
<br>

<div class="container">
    <div class="card">
        <div class="card-body">
            <g:if test="${!parkingPlace}">
                <h2><g:message code="create.place.title"/></h2>
            </g:if>
            <g:else>
                <h2><g:message code="edit.place.title"/></h2>
            </g:else>
            <br>

            <br>
            <g:form url="[controller: 'ParkingPlace', action: 'editInfo']">
                <label for="number"><g:message code="edit.places.placeNumber.label"/></label>
                <br>
                <g:field type="number" name="number" id="number" value="${parkingPlace?.number}"/>
                <br><br>
                <label for="floor"><g:message code="edit.places.floor.label"/></label>
                <br>
                <g:field type="number" name="floor" id="floor" value="${parkingPlace?.floor}"/>
                <br><br>
                <label for="owner"><g:message code="edit.places.owner.label"/></label>
                <br>
                <g:field type="text" name="owner" id="owner" value="${parkingPlace?.owner?.username}"/>
                <br><br>
                <label for="isRented"><g:message code="edit.places.isRented.label"/></label>
                <br>
                <select class="mdb-select md-form" name="isRented" id="isRented">
                    <g:if test="${parkingPlace?.isRented}">
                        <option value="true" selected>
                    </g:if>
                    <g:else>
                        <option value="true">
                    </g:else>
                    <g:message code="list.filters.places.isRented.select.option1"/></option>
                    <g:if test="${parkingPlace?.isRented}">
                        <option value="false">
                    </g:if>
                    <g:else>
                        <option value="false" selected>
                    </g:else>
                    <g:message code="list.filters.places.isRented.select.option2"/></option>
                </select>
                <br><br>
                <label for="rentalPrice"><g:message code="edit.places.rentalPrice.label"/></label>
                <br>
                <g:field type="number decimal" name="rentalPrice" id="rentalPrice" value="${parkingPlace?.rentalPrice}"/>
                <br><br>
                <label for="renter"><g:message code="edit.places.renter.label"/></label>
                <br>
                <g:field type="text" name="renter" id="renter" value="${parkingPlace?.renter?.username}"/>
                <br><br>
                <g:field type="hidden" name="id" id="id" value="${parkingPlace?.id}"/>
                <g:if test="${!parkingPlace}">
                    <g:submitButton name="submit"
                                    value="${message(code: 'default.button.create.label')}"></g:submitButton>
                </g:if>
                <g:else>
                    <g:submitButton name="submit"
                                    value="${message(code: 'default.button.update.label')}"></g:submitButton>
                </g:else>
            </g:form>
        </div>
    </div>
</div>
<br>

<div>
    <h1>FOOTER</h1>
</div>
</body>
</html>
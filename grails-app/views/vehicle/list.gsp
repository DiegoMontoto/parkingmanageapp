<%--
  Created by IntelliJ IDEA.
  User: diegomontoto
  Date: 04/11/2020
  Time: 10:38
--%>

<%@ page import="parkingmanageapp.VehicleType" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="list.vehicles.title"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'ListTable.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
</head>

<body>
<div>
    <h1>HEADER</h1>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <h2><g:message code="list.filters.title"/></h2>
                    <br>

                    <g:form url="[controller: 'Vehicle', action: 'list']">
                        <label for="licencePlate"><g:message code="list.filters.vehicles.licencePlate.label"/></label>
                        <br>
                        <g:field type="text" name="licencePlateNumber" id="licencePlate"/>
                        <br><br>
                        <label for="type"><g:message code="list.filters.vehicles.type.label"/></label>
                        <br>
                        <select class="mdb-select md-form" name="type" id="type">
                            <option value="0" disabled selected><g:message
                                    code="list.filters.vehicles.type.select.default"/></option>
                            <option value="1"><g:message code="list.filters.vehicles.type.select.option1"/></option>
                            <option value="2"><g:message code="list.filters.vehicles.type.select.option2"/></option>
                            <option value="3"><g:message code="list.filters.vehicles.type.select.option3"/></option>
                        </select>
                        <br><br>
                        <label for="owner"><g:message code="list.filters.vehicles.owner.label"/></label>
                        <br>
                        <g:field type="text" name="owner" id="owner"/>
                        <br><br>
                        <g:submitButton name="submit" value="${message(code: 'list.filters.button')}"></g:submitButton>
                    </g:form>
                </div>
            </div>
        </div>

        <div class="col-9">
            <div>
                <h2><g:message code="list.vehicles.title"/></h2>
            </div>
            <table class='vehicle-display-table'>
                <tr>
                    <th><g:message code="list.table.vehicles.c1"/></th>
                    <th><g:message code="list.table.vehicles.c2"/></th>
                    <th><g:message code="list.table.vehicles.c3"/></th>
                    <th><g:message code="list.table.vehicles.c4"/></th>
                </tr>

                <g:each var="vehicle" in="${vehicles}" status="i">
                    <g:if test="${i % 2 == 0}">
                        <tr class="even-row">
                    </g:if>
                    <g:else>
                        <tr class="odd-row">
                    </g:else>
                    <td>${vehicle.licencePlateNumber}</td>
                    <g:if test="${vehicle.type == VehicleType.COCHE}">
                        <td><g:message code="list.filters.vehicles.type.select.option1"/></td>
                    </g:if>
                    <g:elseif test="${vehicle.type == VehicleType.MOTO}">
                        <td><g:message code="list.filters.vehicles.type.select.option2"/></td>
                    </g:elseif>
                    <g:else>
                        <td><g:message code="list.filters.vehicles.type.select.option3"/></td>
                    </g:else>
                    <td><a href="">${vehicle.owner.username}</a></td>
                    <td>
                        <g:link action="edit" params="[id: vehicle?.id]">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="25pt" height="25pt" viewBox="0 0 25 25" version="1.1">
                                <g id="surface1">
                                    <path style=" stroke:none;fill-rule:evenodd;fill:rgb(0%,0%,0%);fill-opacity:1;"
                                          d="M 2.089844 0 L 22.910156 0 C 24.058594 0 25 0.941406 25 2.089844 L 25 22.910156 C 25 24.058594 24.058594 25 22.910156 25 L 2.089844 25 C 0.941406 25 0 24.058594 0 22.910156 L 0 2.089844 C 0 0.941406 0.941406 0 2.089844 0 Z M 10.46875 17.664062 C 10.050781 17.804688 9.625 17.933594 9.207031 18.070312 C 8.792969 18.207031 8.375 18.347656 7.949219 18.488281 C 6.957031 18.808594 6.414062 18.988281 6.292969 19.019531 C 6.175781 19.054688 6.253906 18.59375 6.5 17.632812 L 7.289062 14.613281 L 13.242188 8.421875 L 16.414062 11.476562 Z M 16.402344 6.179688 C 16.253906 6.042969 16.082031 5.96875 15.890625 5.976562 C 15.699219 5.976562 15.527344 6.050781 15.386719 6.203125 L 14.253906 7.375 L 17.425781 10.4375 L 18.566406 9.242188 C 18.707031 9.105469 18.757812 8.921875 18.757812 8.730469 C 18.757812 8.539062 18.683594 8.355469 18.542969 8.226562 Z M 16.402344 6.179688 "/>
                                </g>
                            </svg>
                        </g:link>
                        <g:link action="delete" params="[id: vehicle?.id]">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="25pt" height="28pt" viewBox="0 0 25 28" version="1.1">
                                <g id="surface1">
                                    <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;"
                                          d="M 17.867188 11.199219 L 16.496094 11.199219 L 16.496094 23.933594 L 17.867188 23.933594 Z M 1.398438 2.109375 L 8.824219 2.109375 L 8.824219 1.457031 C 8.824219 1.4375 8.824219 1.414062 8.824219 1.394531 C 8.855469 0.746094 9.339844 0.199219 9.984375 0.078125 C 10.152344 0.046875 10.304688 0.0507812 10.476562 0.0507812 L 14.753906 0.0507812 C 14.808594 0.0507812 14.863281 0.0507812 14.921875 0.0546875 C 15.285156 0.0742188 15.617188 0.226562 15.859375 0.464844 C 16.09375 0.699219 16.246094 1.019531 16.269531 1.371094 C 16.273438 1.410156 16.277344 1.453125 16.277344 1.496094 L 16.277344 2.109375 L 23.777344 2.109375 C 23.878906 2.109375 23.953125 2.109375 24.054688 2.125 C 24.5625 2.207031 24.949219 2.609375 24.992188 3.117188 C 24.996094 3.171875 25 3.226562 25 3.28125 L 25 5.671875 C 25 6.007812 24.726562 6.277344 24.382812 6.277344 L 0.617188 6.277344 C 0.273438 6.277344 0 6.007812 0 5.671875 L 0 3.410156 C 0 2.464844 0.472656 2.109375 1.398438 2.109375 Z M 13.40625 11.199219 L 12.03125 11.199219 L 12.03125 23.933594 L 13.40625 23.933594 Z M 8.945312 11.199219 L 7.570312 11.199219 L 7.570312 23.933594 L 8.945312 23.933594 Z M 2.480469 7.21875 L 22.585938 7.21875 C 22.667969 7.222656 22.738281 7.226562 22.824219 7.242188 C 23.113281 7.289062 23.378906 7.425781 23.578125 7.617188 C 23.8125 7.84375 23.964844 8.148438 23.96875 8.488281 C 23.96875 8.558594 23.964844 8.628906 23.957031 8.703125 L 22.265625 26.5625 C 22.242188 26.796875 22.203125 27.003906 22.082031 27.226562 C 21.847656 27.652344 21.417969 27.949219 20.882812 27.949219 L 4.246094 27.949219 C 4.121094 27.949219 4.003906 27.949219 3.878906 27.929688 C 3.828125 27.925781 3.78125 27.914062 3.734375 27.898438 C 3.660156 27.878906 3.589844 27.851562 3.523438 27.820312 C 3.460938 27.789062 3.402344 27.757812 3.34375 27.71875 C 2.996094 27.480469 2.761719 27.085938 2.722656 26.644531 L 1.039062 8.652344 C 1.035156 8.59375 1.03125 8.53125 1.03125 8.476562 C 1.042969 8.140625 1.191406 7.839844 1.421875 7.617188 C 1.625 7.421875 1.898438 7.28125 2.191406 7.238281 C 2.285156 7.222656 2.386719 7.21875 2.480469 7.21875 Z M 22.566406 8.429688 L 2.445312 8.429688 C 2.316406 8.429688 2.253906 8.453125 2.265625 8.585938 L 3.941406 26.535156 C 3.949219 26.621094 3.988281 26.695312 4.046875 26.726562 L 4.066406 26.730469 L 4.078125 26.734375 L 20.878906 26.734375 C 21.019531 26.734375 21.035156 26.542969 21.042969 26.445312 L 22.738281 8.574219 C 22.746094 8.460938 22.675781 8.429688 22.566406 8.429688 Z M 22.566406 8.429688 "/>
                                </g>
                            </svg>
                        </g:link>
                    </td>
                    </tr>
                </g:each>
            </table>
            <g:link action="create" params="[]">
                <g:message code="list.vehicles.add"/>
            </g:link>
        </div>
    </div>
</div>
<br>

<div>
    <h1>FOOTER</h1>
</div>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: diegomontoto
--%>

<%@ page import="parkingmanageapp.VehicleType" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="edit.vehicle.title"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
</head>

<body>
<div>
    <h1>HEADER</h1>
</div>
<br>

<div class="container">
    <div class="card">
        <div class="card-body">
            <g:if test="${!vehicle}">
                <h2><g:message code="create.vehicle.title"/></h2>
            </g:if>
            <g:else>
                <h2><g:message code="edit.vehicle.title"/></h2>
            </g:else>
            <br>

            <g:form url="[controller: 'Vehicle', action: 'editInfo']">
                <label for="licencePlate"><g:message code="edit.vehicles.licencePlate.label"/></label>
                <br>
                <g:field type="text" name="licencePlateNumber" id="licencePlate"
                         value="${vehicle?.licencePlateNumber}"/>
                <br><br>
                <label for="type"><g:message code="list.filters.vehicles.type.label"/></label>
                <br>
                <select class="mdb-select md-form" name="type" id="type">
                    <g:if test="${vehicle?.type?.id == 1}">
                        <option value="1" selected>
                    </g:if>
                    <g:else>
                        <option value="1">
                    </g:else>
                    <g:message code="list.filters.vehicles.type.select.option1"/></option>
                    <g:if test="${vehicle?.type?.id == 2}">
                        <option value="2" selected>
                    </g:if>
                    <g:else>
                        <option value="2">
                    </g:else>
                    <g:message code="list.filters.vehicles.type.select.option2"/></option>
                    <g:if test="${vehicle?.type?.id == 3}">
                        <option value="3" selected>
                    </g:if>
                    <g:else>
                        <option value="3">
                    </g:else>
                    <g:message code="list.filters.vehicles.type.select.option3"/></option>
                </select>
                <br><br>
                <label for="owner"><g:message code="edit.vehicles.owner.label"/></label>
                <br>
                <g:field type="text" name="owner" id="owner" value="${vehicle?.owner?.username}"/>
                <br><br>
                <g:field type="hidden" name="id" id="id" value="${vehicle?.id}"/>
                <g:if test="${!vehicle}">
                    <g:submitButton name="submit" value="${message(code: 'default.button.create.label')}"></g:submitButton>
                </g:if>
                <g:else>
                    <g:submitButton name="submit" value="${message(code: 'default.button.update.label')}"></g:submitButton>
                </g:else>
            </g:form>
        </div>
    </div>
</div>
<br>

<div>
    <h1>FOOTER</h1>
</div>
</body>
</html>
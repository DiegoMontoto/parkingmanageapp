package parkingmanageapp

import grails.gorm.transactions.Transactional

@Transactional
class VehicleService {

    List<Vehicle> filterVehicles(VehicleType type, String licencePlateNumber, String ownerName) {

        Vehicle.createCriteria().list() {
            if (type) eq("type", type)
            if (licencePlateNumber) ilike("licencePlateNumber", "%${licencePlateNumber}%")
            owner {
                if (ownerName) ilike("username", "%${ownerName}%")
            }
        }
    }

    Vehicle deleteVehicle(Vehicle v) {

        List<ParkingPlace> parkingPlaces = v.parkingPlaces as List
        parkingPlaces.each { ParkingPlace pp ->
            v.removeFromParkingPlaces(pp)
            pp.save()
        }
        v.delete()

        Vehicle.get(v.id)
    }

    Vehicle saveVehicle(VehicleType type, String licencePlateNumber, User o, Long id = null){
        Vehicle vehicle = id ? Vehicle.get(id) : new Vehicle()
        vehicle.type=type
        vehicle.licencePlateNumber=licencePlateNumber
        vehicle.owner=o
        vehicle.save()
    }

}

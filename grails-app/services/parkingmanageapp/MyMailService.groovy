package parkingmanageapp

import grails.gorm.transactions.Transactional

@Transactional
class MyMailService {

    void sendNewVehicleMail(Vehicle v) {
        sendMail {
            async true
            to getAdminMail()
            subject "Nuevo vehículo registrado"
            text """Buenos días,
Se le envía este correo para notificarle de que el vehículo de tipo "${v.type}" con número de matrícula "${v?.licencePlateNumber}" y dueño "${v?.owner?.username}" ha sido registrado correctamente.
Por favor no responda a este correo."""
        }
    }

    void sendNewPlaceMail(ParkingPlace p){
        sendMail{
            async true
            to getAdminMail()
            subject "Nueva plaza registrada"
            text """Buenos días,
Se le envía este correo para notificarle de que la plaza número ${p.number} en el piso ${p.floor} con dueño "${p?.owner?.username}" ha sido registrada correctamente.
Por favor no responda a este correo."""
        }
    }

    private String getAdminMail(){
        Role adminRole = Role.findByAuthority("ROLE_ADMIN")
        UserRole.findByRole(adminRole)?.user?.email
    }
}

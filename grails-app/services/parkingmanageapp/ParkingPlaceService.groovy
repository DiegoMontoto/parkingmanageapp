package parkingmanageapp

import grails.gorm.transactions.Transactional

@Transactional
class ParkingPlaceService {

    List<ParkingPlace> filterParkingPlaces(Integer number, Integer floor, String ownerName, Boolean isRented, String renterName) {

        ParkingPlace.createCriteria().list() {
            if (number) eq("number", number)
            if (floor) eq("floor", floor)
            owner {
                if (ownerName) ilike("username", "%${ownerName}%")
            }
            if (isRented != null) eq("isRented", isRented)
            if (renterName && isRented) renter {
                ilike("username", "%${renterName}%")
            }
        }
    }

    ParkingPlace deletePlace(ParkingPlace p) {
        while (p?.vehicles)
            p.removeFromVehicles(p.vehicles[0])
        p.delete()
        List<Vehicle> vehicles = p.vehicles as List
        vehicles.each { Vehicle v ->
            p.removeFromVehicles(v)
            v.save()
        }
        p.delete()

        ParkingPlace.get(p.id)
    }

    ParkingPlace saveParkingPlace(int number, int floor, float rentalPrice, boolean isRented, User o, User r, Long id = null){
        ParkingPlace p = id ? ParkingPlace.get(id) : new ParkingPlace()
        p.number=number
        p.floor=floor
        p.rentalPrice=rentalPrice
        p.isRented=isRented
        p.owner=o
        p.renter=r
        p.save()
    }
}

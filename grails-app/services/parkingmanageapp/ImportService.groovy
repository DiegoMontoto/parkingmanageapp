package parkingmanageapp

import grails.gorm.transactions.Transactional

@Transactional
class ImportService {

    void importData() {
            importUsers()
            importVehicles()
            importParkingPlaces()
    }
    
    void importUsers() {
        def adminRole
        def userRole
        if(!Role.findByAuthority('ROLE_ADMIN'))
            adminRole = new Role(authority: 'ROLE_ADMIN').save()
        if(!Role.findByAuthority('ROLE_USER'))
            userRole = new Role(authority: 'ROLE_USER').save()
        if(!User.get(1L)){
            User u1 = new User(id:1L,email: 'diego.montoto@salenda.es', password: 'admin', username: 'Admin', phoneNumber: '123456789').save()
            UserRole.create u1,adminRole
            UserRole.withSession {
                it.flush()
                it.clear()
            }
        }
        if(!User.get(2L)){
            User u2 = new User(id:2L,email: 'diego@gmail.com', password: 'password', username: 'Diego', phoneNumber: '987654321').save()
            UserRole.create u2,userRole
            UserRole.withSession {
                it.flush()
                it.clear()
            }
        }

        if(!User.get(3L)){
            User u3 = new User(id:3L,email: 'alberto@gmail.com', password: 'password', username: 'Alberto').save()
            UserRole.create u3,userRole
            UserRole.withSession {
                it.flush()
                it.clear()
            }
        }
    }

    void importVehicles() {
        if(!Vehicle.get(1L))
            new Vehicle(id:1L,licencePlateNumber: '4357 ABD', type: VehicleType.MOTO, owner: User.get(2)).save()
        if(!Vehicle.get(2L))
            new Vehicle(id:2L,licencePlateNumber: '1468 ABD', type: VehicleType.COCHE, owner: User.get(3)).save()
        if(!Vehicle.get(3L))
            new Vehicle(id:3L,licencePlateNumber: '1234 CCD', type: VehicleType.MOTO, owner: User.get(3)).save()
    }

    void importParkingPlaces() {
        if(!ParkingPlace.get(1L))
            new ParkingPlace(id:1L,number: 13, floor: 1, rentalPrice: 350, isRented: false, owner: User.get(2))
                .addToVehicles(Vehicle.get(1))
                .save()
        if(!ParkingPlace.get(2L))
            new ParkingPlace(id:2L,number: 21, floor: 2, rentalPrice: 200, isRented: true, renter: User.get(3),
                owner: User.get(2))
                .addToVehicles(Vehicle.get(2))
                .addToVehicles(Vehicle.get(3))
                .save()
        if(!ParkingPlace.get(3L))
            new ParkingPlace(id:3L,number: 44, floor: 3, rentalPrice: 150, isRented: false, owner: User.get(3))
                .addToVehicles(Vehicle.get(2))
                .addToVehicles(Vehicle.get(3))
                .save()
    }
}

package parkingmanageapp

import grails.util.Environment

class BootStrap {

    ImportService importService

    def init = { servletContext ->
        if (Environment.current != Environment.PRODUCTION && Environment.current != Environment.TEST) {
            importService.importData()
        }
        TimeZone.setDefault(TimeZone.getTimeZone('UTC'))
        Locale.setDefault(new Locale("es"))
    }
    def destroy = {
    }
}

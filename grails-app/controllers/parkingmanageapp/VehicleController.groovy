package parkingmanageapp

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class VehicleController {

    VehicleService vehicleService
    MyMailService myMailService

    def index() {
        redirect(action: 'list')
    }

    def list() {
        VehicleType type = !params?.type ? null : VehicleType.byId(params.type as Integer)
        String owner = params?.owner ?: null
        String licencePlateNumber = params?.licencePlateNumber ?: ""
        [vehicles: vehicleService.filterVehicles(type, licencePlateNumber, owner)]
    }

    def create(){
        render(view: "edit")
    }

    def edit() {
        Vehicle vehicle = Vehicle.get(params.id)
        [vehicle: vehicle]
    }

    def editInfo() {
        VehicleType type = VehicleType.byId(params?.type as Integer)
        String licencePlateNumber = params?.licencePlateNumber
        User owner = User.findByUsername(params?.owner)
        if(owner){
            Vehicle v = vehicleService.saveVehicle(type,licencePlateNumber,owner,params.int("id"))
            if(v&&!params?.id)
                myMailService.sendNewVehicleMail(v)
        }else{
            flash.errors = message(code:'error.vehicle.noOwner')
        }
        redirect(action: "list")
    }

    def delete() {
        Vehicle vehicle = Vehicle.get(params.id)
        if (vehicle) {
            vehicleService.deleteVehicle(vehicle)
        } else {
            flash.errors = 'no hay vehiculo'
        }
        redirect(action: "list")
    }
}

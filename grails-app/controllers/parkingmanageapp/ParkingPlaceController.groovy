package parkingmanageapp

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class ParkingPlaceController {

    ParkingPlaceService parkingPlaceService
    MyMailService myMailService

    def index() {
        redirect(action: 'list')
    }

    def list() {
        Integer number = params?.number ? params.number as int : null
        Integer floor = params?.floor ? params.floor as int : null
        String owner = params?.owner ?: null
        Boolean isRented = params?.isRented == "true" ? true :
                params?.isRented == "false" ? false : null
        String renter = params?.renter ?: null
        [parkingPlaces: parkingPlaceService.filterParkingPlaces(number, floor, owner, isRented, renter)]
    }

    def create() {
        render(view: "edit")
    }

    def edit() {
        ParkingPlace parkingPlace = ParkingPlace.get(params.id)
        [parkingPlace: parkingPlace]
    }

    def editInfo() {
        int number = params.int("number") ?: 0
        int floor = params.int("floor") ?: 0
        float rentalPrice = params.float("rentalPrice") ?: 0
        boolean isRented = params.boolean("isRented")
        User owner = User.findByUsername(params?.owner)
        User renter = isRented ? User.findByUsername(params?.renter) : null
        if (owner) {
            if (!isRented || (isRented && renter)) {
                ParkingPlace p = parkingPlaceService.saveParkingPlace(number, floor, rentalPrice, isRented, owner, renter, params.int("id"))
                if (p && !params?.id)
                    myMailService.sendNewPlaceMail(p)
            } else
                flash.errors = message(code: 'error.parkingPlace.renter')
        } else {
            flash.errors = message(code: 'error.parkingPlace.noOwner')
        }
        redirect(action: "list")
    }

    def delete() {
        ParkingPlace parkingPlace = ParkingPlace.get(params.id)
        if (parkingPlace) {
            parkingPlaceService.deletePlace(parkingPlace)
        } else {
            flash.errors = 'no hay plaza'
        }
        redirect(action: "list")
    }
}

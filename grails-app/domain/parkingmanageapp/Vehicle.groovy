package parkingmanageapp

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes=["licencePlateNumber","type","owner"])
class Vehicle {

    String licencePlateNumber
    VehicleType type
    static hasMany = [parkingPlaces : ParkingPlace]
    static belongsTo =  ParkingPlace
    User owner
    static mapping = {
        owner lazy: false
        parkingPlaces lazy:false
    }

    static constraints = {
        licencePlateNumber(matches: /(?smx) #flgs
                                    (^(\D | \D{2} | \D{3})-\d{6}$)  #sistema provincial numérico (1900-1971)
                                    |(^(\D | \D{2})-\d{4}-\D{2}$)   #sistema provincial alfanumérico (1971-2000)
                                    |(^\d{4}\s\D{3}$)               #sistema nacional (2000-actualidad)
                                    /)
    }
}

enum VehicleType{
    COCHE(1), MOTO(2), FURGONETA(3)
    final Integer id

    VehicleType(Integer id){
        this.id = id
    }

    static VehicleType byId(Integer id) {
        values().find { it.id == id }
    }
}
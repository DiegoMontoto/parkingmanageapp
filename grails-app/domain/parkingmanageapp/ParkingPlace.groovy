package parkingmanageapp

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes=["number","floor","rentalPrice","isRented","owner"])
class ParkingPlace {

    int number
    int floor
    float rentalPrice
    boolean isRented
    User renter
    static hasMany = [vehicles: Vehicle]
    static belongsTo = [owner: User]
    static mapping = {
        owner lazy: false
        renter lazy: false
    }

    static constraints = {
        renter nullable: true, validator: { User renter, ParkingPlace place ->
            (place.isRented && renter) || (!place.isRented && renter == null)
        }
        rentalPrice validator: { float value -> value >= 0 }
    }
}

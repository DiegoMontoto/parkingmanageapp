package parkingmanageapp

import grails.testing.mixin.integration.Integration
import org.springframework.test.annotation.Rollback
import spock.lang.Specification

@Integration
@Rollback
class ImportServiceIntegrationSpec extends Specification {

    ImportService importService

    def "validate the ImportService"() {
        given: 'the data is imported by the importService'
        importService.importData()
        List<User> users
        List<Vehicle> vehicles
        User admin
        List<ParkingPlace> places
        
        when: 'the data is load from the database'
        User.withNewSession { session ->
            users = User.list()
            admin = User.findByUsername('Admin')
            vehicles = Vehicle.list()
            places = ParkingPlace.list()
        }

        then: 'the data has been load correctly'
        users.any { it.username == 'Diego' }
        users.size() == 3
        vehicles.every { it.owner != admin }
        vehicles.size() == 3
        places.every { it.owner != admin }
        places.any { it.renter != null }
        places.size() == 3

    }
}

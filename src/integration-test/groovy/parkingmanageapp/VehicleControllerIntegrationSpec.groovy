package parkingmanageapp

import grails.gsp.PageRenderer
import grails.testing.mixin.integration.Integration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import spock.lang.Unroll

@Integration
@Rollback
class VehicleControllerIntegrationSpec extends BaseControllerIntegrationSpec {

    @Autowired
    VehicleController vehicleController

    ImportService importService
    PageRenderer groovyPageRenderer

    String controllerName = 'vehicle'

    @Unroll
    void "Validate the list method"() {

        given: 'data is imported in the database'
        importService.importData()

        when: 'data is loaded from the database'
        List<Vehicle> vehicles = []
        Vehicle.withNewSession { session ->
            vehicles = Vehicle.list()
        }

        and:
        params.each { k, v ->
            vehicleController.params."${k}" = "${v}"
        }
        and: 'the controller action is call'
        List<Vehicle> vehiclesout = vehicleController.list().vehicles

        then: 'validate the list output'
        vehiclesout == vehicles.findAll { it.id in lista }

        where:
        lista        | params
        [1L, 2L, 3L] | null
        [1L, 3L]     | [type: "2"]
        [1L]         | [owner: "D"]
        [2L]         | [type: "1"]
        [1L]         | [licencePlateNumber: "43"]
        [2L, 3L]     | [owner: "Alberto"]
        [2L]         | [licencePlateNumber: "68", owner: "Alberto"]

    }

    @Unroll
    void "Validate the delete method"() {

        given: 'data is imported in the database'
        importService.importData()
        Vehicle out

        when: 'params are read'
        vehicleController.params.id = id

        and: 'the controller action is call'
        Vehicle.withNewSession {
            out = vehicleController.delete()
        }

        then: 'validate the list output after deleting vehicles'
        out == null

        where:
        id    | _
        '2'   | _
        '123' | _
        '3'   | _
        '1'   | _

    }

    @Unroll
    void "Validate the edit method"() {

        given: 'data is imported in the database'
        importService.importData()
        Vehicle out
        Vehicle expectedout

        when: 'params are read'
        vehicleController.params.id = id

        and: 'the controller action is call'
        Vehicle.withNewSession {
            out = vehicleController.edit().vehicle
            expectedout = Vehicle.get(expectedId)
        }

        then: 'validate the method output'
        out == expectedout

        where:
        id    | expectedId
        null  | null
        '2'   | '2'
        '123' | null
        '3'   | '3'
        '1'   | '1'

    }

    void "Validate the create method"() {

        given: 'data is imported in the database'
        importService.importData()

        and: 'the controller action is call'
        vehicleController.edit().vehicle

        expect: 'validate the method output'
        vehicleController.response.status==200
        vehicleController.response.text== groovyPageRenderer.render(view: "edit")
    }

    void "Validate the editInfo method"() {

        given: 'data is imported in the database'
        importService.importData()

        and: 'the controller action is call'
        Vehicle.withNewSession {
            vehicleController.editInfo()
        }


        expect: 'validate the method output'
        vehicleController.response.status==302
        vehicleController.response.text==groovyPageRenderer.render(view: "list")
    }
}

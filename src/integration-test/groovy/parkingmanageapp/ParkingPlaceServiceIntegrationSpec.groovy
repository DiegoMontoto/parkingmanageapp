package parkingmanageapp

import grails.testing.mixin.integration.Integration
import org.springframework.test.annotation.Rollback
import spock.lang.Specification
import spock.lang.Unroll

@Integration
@Rollback
class ParkingPlaceServiceIntegrationSpec extends Specification {

    ParkingPlaceService parkingPlaceService
    ImportService importService


    @Unroll
    void "Validate the filterParkingPlaces method"() {

        given: 'data is imported in the database'
        importService.importData()

        when: 'data is loaded from the database'
        List<ParkingPlace> parkingPlaces = []
        ParkingPlace.withNewSession { session ->
            parkingPlaces = ParkingPlace.list()
        }

        and: 'the service method is call'
        List<ParkingPlace> out = parkingPlaceService.filterParkingPlaces(number, floor, owner, isRented, renter)

        then: 'validate the filterParkingPlaces output'
        out == parkingPlaces.findAll {
            it.id in lista
        }

        where:
        lista        | number | floor | owner | isRented | renter
        [1L, 2L, 3L] | null   | null  | null  | null     | null
        [1L, 3L]     | null   | null  | null  | false    | null
        [2L]         | null   | null  | null  | true     | null
        [2L]         | null   | null  | null  | true     | "A"
        []           | null   | null  | null  | true     | "aldndbcs"
        [1L, 3L]     | null   | null  | null  | false    | "A"
        [1L, 2L]     | null   | null  | "Die" | null     | null
        [1L]         | null   | 1     | "Die" | null     | null
        []           | null   | 24    | "Die" | null     | null
        [2L]         | 21     | null  | "Die" | null     | null
        [3L]         | 44     | null  | null  | null     | null
    }

    @Unroll
    def "validate the saveParkingPlace method"() {
        given: 'the data is imported by the importService'
        importService.importData()
        ParkingPlace out
        ParkingPlace expectedOutput

        when: 'the service method is call'
        ParkingPlace.withNewSession { session ->
            out = parkingPlaceService.saveParkingPlace(number, floor, rentalPrice, isRented, User.get(idUser), User.get(idRenter), id)
            expectedOutput = ParkingPlace.get(idout)
        }

        then: 'the data is delete correctly'
        if (error)
            out == null
        else
            out == expectedOutput

        where:
        number | floor | rentalPrice | isRented | id   | idUser | idRenter | error | idout
        222    | 2     | 123.5       | false    | 1    | 2      | null     | false | 1
        2544   | 2     | 123.5       | true     | 1    | 2      | null     | true  | 1
        0      | 0     | 0           | false    | null | 3      | null     | false | 4
        345    | 1     | 2           | true     | null | 1      | 3        | false | 5
        345    | 1     | 2           | true     | null | null   | 3        | true  | 5
    }

    @Unroll
    def "validate the deletePlace method"() {
        given: 'the data is imported by the importService'
        importService.importData()
        ParkingPlace out

        when: 'the service method is call'
        ParkingPlace.withNewSession { session ->
            out = parkingPlaceService.deletePlace(ParkingPlace.get(id))
        }

        then: 'the data is delete correctly'
        out == null

        where:
        id  | _
        '2' | _
        '3' | _
        '1' | _
    }
}

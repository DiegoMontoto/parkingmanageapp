package parkingmanageapp

import grails.gsp.PageRenderer
import grails.testing.mixin.integration.Integration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import spock.lang.Unroll

@Integration
@Rollback
class ParkingPlaceControllerIntegrationSpec extends BaseControllerIntegrationSpec {

    @Autowired
    ParkingPlaceController parkingPlaceController

    ImportService importService
    PageRenderer groovyPageRenderer

    String controllerName = 'parkingPlace'

    @Unroll
    void "Validate the list method"() {

        given: 'data is imported in the database'
        importService.importData()

        when: 'data is loaded from the database'
        List<ParkingPlace> parkingPlaces = []
        ParkingPlace.withNewSession { session ->
            parkingPlaces = ParkingPlace.list()
        }

        and: 'the controller action is call'
        params.each { k, v ->
            parkingPlaceController.params."${k}" = "${v}"
        }

        and: 'the controller action is call'
        List<ParkingPlace> parkingPlacesOut = parkingPlaceController.list().parkingPlaces

        then: 'validate the list output'
        parkingPlacesOut == parkingPlaces.findAll {
            it.id in lista
        }
        where:
        lista        | params
        [1L, 2L, 3L] | null
        [1L, 3L]     | [isRented: "false"]
        [2L]         | [isRented: "true"]
        [2L]         | [isRented: "true", renter: "A"]
        []           | [isRented: "true", renter: "aldndbcs"]
        [1L, 3L]     | [isRented: "false", renter: "A"]
        [1L, 2L]     | [owner: "Die"]
        [1L]         | [floor: "1", owner: "Die"]
        []           | [floor: "24", owner: "Die"]
        [2L]         | [number: "21", owner: "Die"]
        [3L]         | [number: "44"]
    }

    @Unroll
    void "Validate the delete method"() {

        given: 'data is imported in the database'
        importService.importData()
        ParkingPlace out

        when: 'params are read'
        parkingPlaceController.params.id = id

        and: 'the controller action is call'
        ParkingPlace.withNewSession {
            out = parkingPlaceController.delete()
        }
        then: 'validate the list output after deleting parking places'
        out == null

        where:
        id    | _
        null  | _
        '2'   | _
        '123' | _
        '3'   | _
        '1'   | _

    }

    @Unroll
    void "Validate the edit method"() {

        given: 'data is imported in the database'
        importService.importData()
        ParkingPlace out
        ParkingPlace expectedout

        when: 'params are read'
        parkingPlaceController.params.id = id

        and: 'the controller action is call'
        ParkingPlace.withNewSession {
            out = parkingPlaceController.edit().parkingPlace
            expectedout = ParkingPlace.get(expectedId)
        }
        then: 'validate the method output'
        out == expectedout

        where:
        id    | expectedId
        null  | null
        '2'   | '2'
        '123' | null
        '3'   | '3'
        '1'   | '1'

    }

    void "Validate the create method"() {

        given: 'data is imported in the database'
        importService.importData()

        and: 'the controller action is call'
        parkingPlaceController.edit()

        expect: 'validate the method output'
        parkingPlaceController.response.status==200
        parkingPlaceController.response.text== groovyPageRenderer.render(view: "edit")
    }

    void "Validate the editInfo method"() {

        given: 'data is imported in the database'
        importService.importData()

        and: 'the controller action is call'
        Vehicle.withNewSession {
            parkingPlaceController.editInfo()
        }


        expect: 'validate the method output'
        parkingPlaceController.response.status==302
        parkingPlaceController.response.text==groovyPageRenderer.render(view: "list")
    }
}

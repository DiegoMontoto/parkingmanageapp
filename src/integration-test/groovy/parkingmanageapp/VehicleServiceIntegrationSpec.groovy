package parkingmanageapp

import grails.testing.mixin.integration.Integration
import org.springframework.test.annotation.Rollback
import spock.lang.Specification
import spock.lang.Unroll

@Integration
@Rollback
class VehicleServiceIntegrationSpec extends Specification {

    VehicleService vehicleService
    ImportService importService


    @Unroll
    void "Validate the filterVehicles method"() {

        given: 'data is imported in the database'
        importService.importData()

        when: 'data is loaded from the database'
        List<Vehicle> vehicles = []
        Vehicle.withNewSession { session ->
            vehicles = Vehicle.list()
        }
        and: 'the service method is call'
        List<Vehicle> out = vehicleService.filterVehicles(type, plate, owner)

        then: 'validate the filterVehicles output'
        out == vehicles.findAll { it.id in lista }

        where:
        lista        | type              | plate | owner
        [1L, 2L, 3L] | null              | null  | null
        [1L, 3L]     | VehicleType.MOTO  | null  | null
        [1L]         | null              | null  | "D"
        [2L]         | VehicleType.COCHE | null  | null
        [1L]         | null              | "43"  | null
        [2L, 3L]     | null              | null  | "Alberto"
        [2L]         | null              | "68"  | "Alberto"

    }

    @Unroll
    def "validate the saveVehicle method"() {
        given: 'the data is imported by the importService'
        importService.importData()
        Vehicle out
        Vehicle expectedOutput

        when: 'the service method is call'
        Vehicle.withNewSession { session ->
            out = vehicleService.saveVehicle(type,plateNumber, User.get(idUser),id)
            expectedOutput = Vehicle.get(idout)
        }

        then: 'the data is delete correctly'
        if (error)
            out == null
        else
            out == expectedOutput

        where:
        id   | type                  | plateNumber | idUser | error | idout
        2    | VehicleType.COCHE     | "1234 ADF"  | 2      | false | 2
        2    | VehicleType.COCHE     | "1234 ADF"  | 12     | true  | 2
        2    | VehicleType.COCHE     | "1234 A2"   | 1      | true  | 2
        3    | VehicleType.FURGONETA | "1234 ADF"  | 1      | false | 3
        3    | VehicleType.FURGONETA | "1234 ADF"  | null   | true  | 3
        null | VehicleType.FURGONETA | "1234 ADF"  | 1      | false | 4
    }

    @Unroll
    def "validate the deleteVehicle method"() {
        given: 'the data is imported by the importService'
        importService.importData()
        Vehicle out

        when: 'the service method is call'
        Vehicle.withNewSession { session ->
            out = vehicleService.deleteVehicle(Vehicle.get(id))
        }

        then: 'the data is delete correctly'
        out == null

        where:
        id | _
        2  | _
        3  | _
        1  | _
    }

}

package parkingmanageapp

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Unroll

class ParkingPlaceSpec extends Specification implements DomainUnitTest<ParkingPlace> {

    @Unroll
    def "validate a ParkingPlace object"() {
        given: 'a new parkingPlace with params'
        params.owner = ownerExists ? new User(email: 'diego@gmail.com', password: 'password', username: 'Diego', phoneNumber: 98765432, role: 'USER_ROLE') : null
        params.renter = renterExists ? new User(email: 'alberto@gmail.com', password: 'password', username: 'Alberto', role: 'USER_ROLE') : null
        ParkingPlace place = new ParkingPlace(params)
        expect: 'validate the object'
        place.validate() == validateRef
        where: 'params are...'
        ownerExists | renterExists | params                                                     | validateRef
        false       | false        | [:]                                                        | false
        true        | false        | [number: 13, floor: 1, rentalPrice: 350, isRented: false]  | true
        true        | false        | [number: 13, floor: 1, rentalPrice: -350, isRented: false] | false
        true        | true         | [number: 13, floor: 1, rentalPrice: 350, isRented: true]   | true
        true        | true         | [number: 13, floor: 1, rentalPrice: 350, isRented: false]  | false
        true        | false        | [number: 13, floor: 1, rentalPrice: 350, isRented: true]   | false
        false       | true         | [number: 13, floor: 1, rentalPrice: 350, isRented: true]   | false

    }
}

package parkingmanageapp

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Unroll

class VehicleSpec extends Specification implements DomainUnitTest<Vehicle> {

    @Unroll
    def "validate a Vehicle object"() {
        given: 'a new vehicle with params'
        params.owner = ownerExists ? new User(email: 'diego@gmail.com', password: 'password', username: 'Diego', phoneNumber: 98765432, role: 'USER_ROLE') : null
        Vehicle vehicle = new Vehicle(params)
        expect: 'validate the object'
        vehicle.validate() == validateRef
        where: 'params are...'
        ownerExists | params                                                        | validateRef
        false       | [:]                                                           | false
        true        | [licencePlateNumber: '4357 ABD', type: VehicleType.MOTO]      | true
        false       | [licencePlateNumber: '4357 ABD', type: VehicleType.MOTO]      | false
        true        | [licencePlateNumber: '4357 ABD', type: 'BADTYPE']             | false
        true        | [licencePlateNumber: 'BADPLATE', type: VehicleType.FURGONETA] | false
    }
}
